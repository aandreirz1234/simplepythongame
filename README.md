# SimplePythonGame

This is a simple python platformer style game that utlises the pygame library as the main backbone 

## aMain

aMain is the main script for the game and consists of ~800 lines of OOP definitions and ~700 of game state checks that ensure the game functions

## Levels

The 2 CSV files, level1_data and level2_data are the level definitions. Each number within the file represents either en empty space or a specific type of tile that needs to be displayed to the user

## Map Editor

LevelEditor.py is the main script that is used to allow you as the developerm to change, add and delete levels for the game and any changes made to the levels are instantly made available when aMain is ran.

NOTE: This is not my code, the original github page can be found here: https://github.com/russs123/LevelEditor I have only edited this to satsify my needs for the game

 
