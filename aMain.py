# importing libraries
import pygame
from pygame import mixer
from threading import Timer
import os
import csv
import re

# initializing pygame
pygame.init()

# background music
bgSound = mixer.Sound("sound/bgSound.wav")
bgSound.set_volume(0.1)
bgSound.play(-1)

# sound effects
DeadEnemySound = mixer.Sound("sound/deadEnemy.wav")
jumpSound = mixer.Sound("sound/jump.wav")
powerupSound = mixer.Sound("sound/Powerup.wav")
coinSound = mixer.Sound("sound/coin.wav")
hitSound = mixer.Sound("sound/hitSound.wav")
swordPower = mixer.Sound("sound/swordPower.wav")
btnSound = mixer.Sound("sound/BTNsound.wav")
gameOverSound = mixer.Sound("sound/gameOverSound.wav")

# screen
SCREEN_WIDTH = 800
SCREEN_HEIGHT = int(SCREEN_WIDTH * 0.8 + 30)
screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
screen = pygame.display.get_surface()
pygame.display.set_caption("platform Kingdom")

# Frame-rate
clock = pygame.time.Clock()
FPS = 60

# constants and variables
userName = ""
item_type = ""
Gravity = 0.75
SCROLL_THRESH = 200
ROWS = 16
COLS = 150
TILE_SIZE = SCREEN_HEIGHT // ROWS
TILE_TYPES = 21
screen_scroll = 0
bg_scroll = 0
level = 1
player_lives = 3
start_time = 0
begin_time = 0
time_enter = 0
time_leave = 0
timeNotPlaying = 0
playerScale = 11
finalTime = 0
enemyLives = 4
rank = 0
timesRemove = []

# setting my boolean variables
game_start = False
paused = False
notInMenu = False
InPaused = False
restart = False
restartAfter = False
restartInPause = False
confirmRestart = False
moving_left = False
moving_right = False
attacking = False
gotUserName = False
boxActive = False
fadeOutMenu = False
fadeOutConfirm = False
startCalc = False
displayedLeaderboard = False
finishedLevelRestart = False
inLeaderboard = False
inInstruction = False
inLookingUsername = False
searchResult = False
found = False
notfound = False
inShop = False
shopMove = False
boughtNuke = False
boughtUnlock = False
backToLevel1 = False
clean = True
clickedOut = False
usernameTaken = False

# colours
WHITE = (255, 255, 255)
RED = (255, 0, 0)

# pattern for input checks
pattern = re.compile(r"[A-Za-z0-9]+")

# fonts and texts
font = pygame.font.Font('freesansbold.ttf', 32)
font2 = pygame.font.Font('freesansbold.ttf', 50)
arial = pygame.font.Font('freesansbold.ttf', 27)
text = font.render('Welcome To Platform Kingdom', True, (0, 0, 0))
text2 = font.render('Enter Username below', True, (0, 0, 0))
text4 = font2.render('Game Over', True, (0, 0, 0))
text5 = font2.render('LEADERBOARD', True, (0, 0, 0))
text6 = font2.render('RANK \t TIME \t NAME ', True, (0, 0, 0))
textRect = text.get_rect()
textRect.center = (SCREEN_WIDTH // 2, SCREEN_HEIGHT // 2 - 200)
textRect2 = text2.get_rect()
textRect2.center = (SCREEN_WIDTH // 2, SCREEN_HEIGHT // 2 - 100)
textRect4 = text4.get_rect()
textRect4.center = (SCREEN_WIDTH // 2, SCREEN_HEIGHT // 2 - 250)
textRect5 = text5.get_rect()
textRect5.center = (SCREEN_WIDTH // 2, SCREEN_HEIGHT // 2 - 250)
textRect6 = text6.get_rect()
textRect6.center = (SCREEN_WIDTH // 2, SCREEN_HEIGHT // 2 - 250)

# text box stuff
userText = ""
searchText = ""
inputRect = pygame.Rect((SCREEN_WIDTH // 2) - 70, (SCREEN_HEIGHT // 2), 140, 50)
searchRect = pygame.Rect((SCREEN_WIDTH // 2) - 70, 300, 140, 50)
colourActive = pygame.Color("gray")
colourPassive = pygame.Color("black")
colour = colourPassive

# loading images for the tile map and background
img_list = []
for x in range(TILE_TYPES):
    img = pygame.image.load(f"tile/{x}.png").convert_alpha()
    img = pygame.transform.scale(img, (TILE_SIZE, TILE_SIZE))
    img_list.append(img)

Bg1 = pygame.image.load("background/forest.png").convert_alpha()
menuBG = pygame.image.load("background/menuBG].png").convert_alpha()

# Heart images for lives
threeLives = pygame.image.load("lives/3Hearts.png").convert_alpha()
twoLives = pygame.image.load("lives/2Hearts.png").convert_alpha()
oneLives = pygame.image.load("lives/1Heart.png").convert_alpha()
NoLives = pygame.image.load("lives/0Hearts.png").convert_alpha()

# powerup text
jumpPowerText = pygame.image.load("collectibles/JumpPower.png").convert_alpha()
instantKillText = pygame.image.load("collectibles/instantKill.png").convert_alpha()
powerText = pygame.image.load("collectibles/blackText.png").convert_alpha()

# shop background
shopBG = pygame.image.load("collectibles/shopImage.png").convert_alpha()

# collectibles dictionary
coin_counter = pygame.image.load("collectibles/coin_counter.png").convert_alpha()
mushroom_img = pygame.image.load("collectibles/mushroom.png").convert_alpha()
black_img = pygame.image.load("collectibles/black.png").convert_alpha()
coin_img = pygame.image.load("collectibles/coin.png").convert_alpha()
kill_img = pygame.image.load("collectibles/superKill.png").convert_alpha()
shop_img = pygame.image.load("collectibles/shop.png").convert_alpha()
goldPile = pygame.image.load("tile/11.png").convert_alpha()
ice = pygame.image.load("tile/14.png").convert_alpha()

collectibles = {
    "coin": coin_img,
    "mushroom": mushroom_img,
    "kill": kill_img,
    "shop": shop_img,
    "gold": goldPile,
    "ice": ice
}

# loading button images and creating their dictionary
backBtn = pygame.image.load("buttons/back.png").convert_alpha()
confirmBtn = pygame.image.load("buttons/confirm.png").convert_alpha()
continueBtn = pygame.image.load("buttons/continue.png").convert_alpha()
exitBtn = pygame.image.load("buttons/exit.png").convert_alpha()
gameoverBtn = pygame.image.load("buttons/gameover.png").convert_alpha()
pauseBtn = pygame.image.load("buttons/pause.png").convert_alpha()
restartBtn = pygame.image.load("buttons/restart.png").convert_alpha()
resumeBtn = pygame.image.load("buttons/resume.png").convert_alpha()
startBtn = pygame.image.load("buttons/start.png").convert_alpha()
playAgainBtn = pygame.image.load("buttons/playagain.png").convert_alpha()
newUserBtn = pygame.image.load("buttons/newuser.png").convert_alpha()
leaderboardBtn = pygame.image.load("buttons/topScore.png").convert_alpha()
instructionBtn = pygame.image.load("buttons/instructions.png").convert_alpha()
searchBtn = pygame.image.load("buttons/search.png").convert_alpha()
shopBtn = pygame.image.load("buttons/shop.png").convert_alpha()
buyBtn = pygame.image.load("buttons/buy.png").convert_alpha()

buttonTypes = {
    "back": backBtn,
    "confirm": confirmBtn,
    "continue": continueBtn,
    "exit": exitBtn,
    "gameover": gameoverBtn,
    "pause": pauseBtn,
    "restart": restartBtn,
    "resume": resumeBtn,
    "start": startBtn,
    "newUser": newUserBtn,
    "playAgain": playAgainBtn,
    "leaderboard": leaderboardBtn,
    "instructions": instructionBtn,
    "search": searchBtn,
    "shop": shopBtn,
    "buy": buyBtn
}


# drawing background
def draw_bg():
    width = Bg1.get_width()
    for x in range(3):
        screen.blit(Bg1, ((x * width) - (bg_scroll * 0.4), 40))


# game restart function
def reset():
    # this clears the groups of objects when the game is restarted to prevent overlapping or duplicated objects
    water_group.empty()
    Item_group.empty()
    exit_group.empty()
    enemy_group.empty()
    ice_barrier.empty()
    # this creates a new world list to restart the position of the map
    data = []
    for row in range(ROWS):
        r = [-1] * COLS
        data.append(r)
    return data


# pause function
def pause():
    # fading elements to the pause function
    fade = pygame.Surface((SCREEN_WIDTH, SCREEN_HEIGHT))
    fade.fill((0, 0, 0))
    blank = pygame.Surface([SCREEN_WIDTH, SCREEN_HEIGHT], pygame.SRCALPHA)
    if paused:
        fade.set_alpha(150)
        screen.blit(fade, (0, 0))
    if paused is False:
        screen.blit(blank, (0, 0))


# fade in and out functions
def fade():
    entryFade = pygame.Surface((SCREEN_WIDTH, SCREEN_HEIGHT))
    entryFade.fill((0, 0, 0))

    # this is the function that control the fade.
    for alpha in range(0, 260):
        # it redraws the window 260 times, each time with a lower opacity to make it seem to fade
        entryFade.set_alpha(alpha)
        redrawWindow()
        screen.blit(entryFade, (0, 0))
        pygame.display.update()


def redrawWindow():
    # part of the fade function. This redraws the window with the initial opacity to prevent the fade from fading to quick
    screen.blit(menuBG, (0, 0))


# gravity reset function for jump power up
def gravReset():
    global Gravity
    Gravity = 0.75
    screen.blit(powerText, (200, 0))


# text if user has not collected enough coins to proceed to next level
def exitErrorSign():
    message = font.render('You do not have sufficient coins', True, (255, 255, 0))
    messageRect = message.get_rect()
    messageRect.center = (SCREEN_WIDTH // 2, 100)
    screen.blit(message, messageRect)


def restartNow():
    # restart function
    global screen_scroll, bg_scroll, player, player_lives, world, backToLevel1, level
    screen_scroll = 0
    temp_lives = 0
    temp_lives = player.lives
    # covering up old text in the toolbar
    screen.blit(powerText, (300, 0))
    screen.blit(powerText, (200, 0))
    screen.blit(black_img, (160, 5))
    # has user restarted to go back to the start of the game
    if backToLevel1:
        level = 1
    if restart or restartInPause:
        # stop the background from moving
        bg_scroll = 0

        # calling the reset function which returns an empty list ready to have new values entered
        world_data = reset()

        # reading the file and assign the table with the values from the file
        with open(f'level{level}_data.csv', newline='') as csvfile:
            reader = csv.reader(csvfile, delimiter=',')
            for x, row in enumerate(reader):
                for y, tile in enumerate(row):
                    world_data[x][y] = int(tile)

        # if the game restarted in these conditions, reset the lives to 3
        if player.gameOver or restartInPause or finishedLevelRestart:
            player_lives = 3

        # if the user fell into the water
        if confirmRestart:
            player_lives -= 1

        # creating new instances of the main features of the game
        world = World()
        player = world.process_data(world_data)

        # user restarted hence they are not finished with the level
        player.completedLevel = False


# sprite class
class User(pygame.sprite.Sprite):
    def __init__(self, char_type, x, y, scale, speed, player_lives, enemyLives):
        pygame.sprite.Sprite.__init__(self)
        # these are the class attributes
        self.lives = player_lives
        self.enemyLives = enemyLives
        self.char_type = char_type
        self.speed = speed
        self.alive = True
        self.gameOver = False
        self.jump = False
        self.in_air = True
        self.jumpPower = False
        self.flip = False
        self.finishedGame = False
        self.facingSame = False
        self.superSword = False
        self.inShop = False
        self.nuked = False
        self.jackpot = False
        self.completedLevel = False
        self.enemyCollisionAdvantage = False
        self.playerCollisionAdvantage = False
        self.direction = 1
        self.vel_y = 0
        self.index = 0
        self.action = 0
        self.coins = 0
        self.animate_list = []
        self.update_time = pygame.time.get_ticks()

        # enemy exclusive attributes
        self.enemy_HB = pygame.Rect(0, 0, 40, 20)
        self.moveBack = False

        # not my code ( quoted in sources in the appendixes)
        # loading the different frames of the animations
        animate_types = ["Idle", "Run", "Jump", "Attack", "Dead"]
        for animate in animate_types:
            temp_list = []
            num_of_img = len(os.listdir(f"png/{self.char_type}/{animate}"))
            for i in range(num_of_img):
                img = pygame.image.load(f"png/{self.char_type}/{animate}/{i}.png")
                img = pygame.transform.scale(img, (int(img.get_width() / (scale)), int(img.get_height() / (scale))))
                temp_list.append(img)
            self.animate_list.append(temp_list)

        self.animate_list.append(temp_list)
        self.image = self.animate_list[self.action][self.index]
        self.rect = self.image.get_rect()
        self.rect.center = (x, y)
        self.width = self.image.get_width()
        self.height = self.image.get_height()

    def move(self, moving_left, moving_right):
        # mostly my code below

        screen_scroll = 0
        dx = 0
        dy = 0

        # moving left and right feature
        if moving_left:
            # reduces x position by the set speed of the sprite
            dx = -self.speed
            self.flip = True
            self.direction = -1
        if moving_right:
            dx = self.speed
            self.flip = False
            self.direction = 1

        # jumping
        if self.jump and self.in_air is False:
            self.vel_y = -11
            self.in_air = True
            self.jump = False

        # this increases the y pos by the gravity (i.e moving down or falling back down)
        self.vel_y += Gravity
        if self.vel_y > 10:
            self.vel_y = 10
        # increments the y pos of the char by the jump velocity var
        dy += self.vel_y

        # collision with the tiles, Ie collision with ground and sides and above and below
        for tile in world.obstacle_list:
            # if the rect of the tile (tile[1] collides with player rect x with "width" distance away, stop movement in that direction
            if tile[1].colliderect(self.rect.x + dx, self.rect.y, self.width, self.height):
                dx = 0
                # if an enemy hits the wall, it auto switches direction
                if self.char_type == 'enemy':
                    self.direction *= -1
            # if the rect of the tile collides with the y pos of the character (going upwards), make the sprite fall back down
            if tile[1].colliderect(self.rect.x, self.rect.y + dy, self.width, self.height):
                if self.vel_y < 0:
                    self.vel_y = 0
                    dy = tile[1].bottom - self.rect.top
                    self.jump = False
                # if the user lands on a platform, acknowledge it as a new ground level.
                elif self.vel_y >= 0:
                    self.vel_y = 0
                    self.in_air = False
                    dy = tile[1].top - self.rect.bottom

        # preventing the sprite from walking of the edge and jumping outside the screen
        if self.char_type == "player":
            if self.lives == 0:
                self.alive = False
            if self.rect.left + dx < 0 or self.rect.right + dx > SCREEN_WIDTH:
                dx = 0
            # prevents user from jumping above screen. if they do touch the top, it sends player back down to the ground
            if self.rect.top + dy < 50:
                self.vel_y = 1

            # check if the user has fallen below the screen
            if self.alive:
                if self.rect.bottom > SCREEN_HEIGHT:
                    # setting dead vars accordingly
                    dy = 0
                    self.alive = False
                    self.lives -= 1
                    self.lostlife = True
                    if self.lives == 0:
                        self.gameOver = True
                        self.kill()

        # checking if the user bought the nuke powerup to kill all instances of enemies
        if self.char_type == "enemy":
            if self.nuked:
                self.kill()
            # if the user killed the enemy, the death animation will play
            if self.alive is False:
                self.update_action(4)

        # player only checks
        if self.char_type == "player":
            # if user has no more lives, game over
            if self.lives == 0:
                redrawWindow()
                fade()
                self.gameOver = True
                self.lives -= 1

        # adjusting the x and y pos based on movement
        self.rect.x += dx
        self.rect.y += dy

        # checking if the screen needs to be moved if the player has gone far enough across the current screen
        if self.char_type == "player":
            # THRESH is the distance the user needs to be away from the right or left of the screen before the screen actually moves
            # if they are in the scroll_thresh zone the screen scroll moves at the same speed as the user (changing by dx (user pos))
            if (self.rect.right > SCREEN_WIDTH - SCROLL_THRESH and bg_scroll < (
                    world.level_length * TILE_SIZE) - SCREEN_WIDTH) \
                    or (self.rect.left < SCROLL_THRESH and bg_scroll > abs(dx)):
                self.rect.x -= dx
                screen_scroll = -dx

        return screen_scroll, self.lives

    def enemies(self):
        # enemy Hit box being created
        self.enemy_HB.center = (self.rect.centerx + 1 * self.direction, self.rect.centery)

        if self.alive:
            self.update_action(1)
        else:
            self.update_action(4)
        # AI movement
        if self.direction == 1:
            ai_moving_right = True
        else:
            ai_moving_right = False
        ai_moving_left = not ai_moving_right
        self.move(ai_moving_left, ai_moving_right)

        # updating enemy rect pos based on movement
        self.rect.x += screen_scroll

    def update_animate(self):
        # not my code ( quoted in sources in the appendixes)
        animate_cooldown = 100
        self.image = self.animate_list[self.action][self.index]

        if pygame.time.get_ticks() - self.update_time > animate_cooldown:
            self.update_time = pygame.time.get_ticks()
            self.index += 1
        if self.index >= len(self.animate_list[self.action]):
            if self.action == 4:
                self.index = len(self.animate_list[self.action]) - 1
                # my addition to the code. this checks if the char type is enemy and if the death aniamtion was played,
                # it sets the death variables accordingly
                if self.char_type == "enemy":
                    self.speed = 0
                    self.alive = False
                    self.kill()
            else:
                self.index = 0

    def update_action(self, new_action):
        # not my code ( quoted in sources in the appendixes)
        if new_action != self.action:
            self.action = new_action
            self.index = 0
            self.update_time = pygame.time.get_ticks()
        if self.char_type == "enemy":
            if new_action == 4:
                self.kill()

    def update(self):
        # not my code ( quoted in sources in the appendixes)
        self.update_animate()

        if self.lives == 0:
            self.alive = False

    def draw(self):
        # not my code ( quoted in sources in the appendixes)
        screen.blit(pygame.transform.flip(self.image, self.flip, False), self.rect)


# world-class that handles processing what tile number corresponds to what tile image
class World():
    def __init__(self):

        # list of all the "hard" obstacles
        self.obstacle_list = []

    def process_data(self, data):

        self.level_length = len(data[0])
        # iterating through each value in the file
        for y, row in enumerate(data):
            for x, tile in enumerate(row):
                # We only want to process tiles not empty spaces
                if tile >= 0:
                    # the number stored in the tile is the index used to retrieve the image from the image list
                    img = img_list[tile]
                    # creating the rect of the tile
                    img_rect = img.get_rect()
                    #  getting the x and y position based on their position in the table of values in the file
                    img_rect.x = x * TILE_SIZE
                    img_rect.y = y * TILE_SIZE + 15
                    # creating a tuple that will contain the image itself and the rectangle for that specific tile
                    tile_data = (img, img_rect)
                    # checking each possible tile number and assigning an action if it is detected
                    if 0 <= tile <= 8:
                        self.obstacle_list.append(tile_data)
                    elif tile == 9 or tile == 10:
                        water = Water(img, x * TILE_SIZE, y * TILE_SIZE + 20)
                        water_group.add(water)
                    elif tile == 11:
                        collectible = Items("gold", x * TILE_SIZE, y * TILE_SIZE + 17)
                        Item_group.add(collectible)
                    elif tile == 12:
                        collectible = Items("shop", x * TILE_SIZE, y * TILE_SIZE + 17)
                        Item_group.add(collectible)
                    elif tile == 13:
                        collectible = Items("kill", x * TILE_SIZE, y * TILE_SIZE + 17)
                        Item_group.add(collectible)
                    elif tile == 14:
                        ice = IceBarrier(img, x * TILE_SIZE, y * TILE_SIZE + 15)
                        ice_barrier.add(ice)
                    elif tile == 15:
                        player = User("player", (x * TILE_SIZE), (y * TILE_SIZE), 11, 5, player_lives, enemyLives)
                        sprite_group.add(player)
                    elif tile == 16:
                        enemySprite = User("enemy", (x * TILE_SIZE), (y * TILE_SIZE), 8.5, 1, player_lives, enemyLives)
                        enemy_group.add(enemySprite)
                    elif tile == 18:
                        collectible = Items("coin", x * TILE_SIZE, y * TILE_SIZE + 17)
                        Item_group.add(collectible)
                    elif tile == 19:
                        collectible = Items("mushroom", x * TILE_SIZE, y * TILE_SIZE + 17)
                        Item_group.add(collectible)
                    elif tile == 20:
                        exit = Exit(img, x * TILE_SIZE, y * TILE_SIZE + 17)
                        exit_group.add(exit)
        return player

    def draw(self):
        # this loops through the obstacle list and blit it to ths screen
        for tile in self.obstacle_list:
            tile[1][0] += screen_scroll
            # tile[0] is the image and tile[1] is the rect from the tuple
            screen.blit(tile[0], tile[1])


# water tile class
class Water(pygame.sprite.Sprite):
    def __init__(self, img, x, y):
        pygame.sprite.Sprite.__init__(self)
        self.image = img
        self.rect = self.image.get_rect()
        # setting the x and y position
        self.rect.midtop = (x + TILE_SIZE // 2 + 1, y + (TILE_SIZE - self.image.get_height()))

    def update(self):
        # enabling it to move along with the screen when the user moves past it
        self.rect.x += screen_scroll


# class for cave barriers
class IceBarrier(pygame.sprite.Sprite):
    def __init__(self, img, x, y):
        pygame.sprite.Sprite.__init__(self)
        self.image = img
        self.rect = self.image.get_rect()
        # setting the x and y position
        self.rect.midtop = (x + TILE_SIZE // 2 + 1, y + (TILE_SIZE - self.image.get_height()))

    def update(self):
        # enabling it to move along with the screen when the user moves past it
        self.rect.x += screen_scroll

        if pygame.sprite.collide_rect(self, player):
            player.lives -= 1
            player.rect.x += 50


# Exit level --> complete level gateway
class Exit(pygame.sprite.Sprite):
    def __init__(self, img, x, y):
        pygame.sprite.Sprite.__init__(self)
        self.image = img
        self.rect = self.image.get_rect()
        # setting the x and y position
        self.rect.midtop = (x + TILE_SIZE // 2, y + (TILE_SIZE - self.image.get_height()))

    def update(self):
        # enabling it to move along with the screen when the user moves past it
        self.rect.x += screen_scroll
        # only let player finish if they have collected 10 or more coins or have the jackpot stored
        if player.coins >= 10 or player.jackpot:
            if pygame.sprite.collide_rect(self, player):
                player.completedLevel = True
                redrawWindow()
                fade()
                self.kill()
        else:
            if pygame.sprite.collide_rect(self, player):
                exitErrorSign()


# collectible items class
class Items(pygame.sprite.Sprite):
    def __init__(self, type, x, y):
        pygame.sprite.Sprite.__init__(self)
        # checking what type of object is needed and assigning it to the attribute
        self.type = type
        self.image = collectibles[self.type]
        # creating the rect (position) of the sprite
        self.rect = self.image.get_rect()
        self.rect.midtop = (x + TILE_SIZE // 2, y + (TILE_SIZE - self.image.get_height()))
        self.boughtUnlock = False

    def update(self):
        # moving object with the screen
        self.rect.x += screen_scroll

        # checking what object was collided with and assigning relevant action to that
        if pygame.sprite.collide_rect(self, player):
            if self.type == "mushroom":
                # super jump power
                player.jumpPower = True
                screen.blit(jumpPowerText, (200, 0))
                powerupSound.set_volume(1)
                powerupSound.play()
                self.kill()
            elif self.type == "coin":
                # coins
                screen.blit(black_img, (165, 5))
                player.coins += 1
                coinSound.set_volume(1)
                coinSound.play()
                self.kill()
            elif self.type == "kill":
                # sword powerup
                screen.blit(instantKillText, (300, 0))
                player.superSword = True
                swordPower.set_volume(1)
                swordPower.play()
                self.kill()
            elif self.type == "shop":
                # shop icon
                player.inShop = True
            elif self.type == "gold":
                # jackpot icon
                player.jackpot = True
                self.kill()

    def getType(self):
        return self.type


# button class that creates buttons
class button():
    def __init__(self, buttonType, x, y, scale):
        self.pressed = False
        # selecting image based on the call and taking it from the dictionary at the start
        self.image = buttonTypes[buttonType]
        # creating height and widths
        self.width = self.image.get_width()
        self.height = self.image.get_height()
        # scaling button
        self.image = pygame.transform.scale(self.image, (int(self.width * scale), int(self.height * scale)))
        # creating rect positions
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y
        # assigning it a position on the screen ( this will be passed in when the an instance is created)
        self.rect.topleft = (x, y)

    def draw(self, surface):
        # no click has been made
        event = False

        # getting the pos of the mouse relative to the screen
        pos = pygame.mouse.get_pos()

        # checking if the button has collided with the position of the mouse
        if self.rect.collidepoint(pos):
            # if the left click has been pressed
            if pygame.mouse.get_pressed()[0] == 1 and self.pressed is False:
                # click has happened
                event = True
                # sound effect
                btnSound.set_volume(0.5)
                btnSound.play()
                self.pressed = True

            # if mouse is not clicked, set clicked tracker to false
            if pygame.mouse.get_pressed()[0] == 0:
                self.pressed = False

        # draw the button on the screen
        surface.blit(self.image, (self.rect.x, self.rect.y))

        return event


# creating empty 2d list to store tile map
world_data = []
for row in range(ROWS):
    # creating an empty table with values of -1 stored inside
    r = [-1] * COLS
    world_data.append(r)

# reading data from csv file and inserting into 2d array
with open(f'level{level}_data.csv', newline='') as csvfile:
    # reading the data from the file. We know each single data as they are separated by a, ""
    reader = csv.reader(csvfile, delimiter=',')
    # iterating through the table in the file
    for x, row in enumerate(reader):
        for y, tile in enumerate(row):
            # The table is a 2d array meaning that we know were we are in the table at all times
            # We then assign the tile number to the world data list in the same position as in the file
            world_data[x][y] = int(tile)

# creating groups for the classes
water_group = pygame.sprite.Group()
exit_group = pygame.sprite.Group()
Item_group = pygame.sprite.Group()
enemy_group = pygame.sprite.Group()
sprite_group = pygame.sprite.Group()
ice_barrier = pygame.sprite.Group()

# start menu buttons
Start = button("start", 100, 250, 1)
Menuexit = button("exit", 550, 250, 1)
instruction = button("instructions", 325, 250, 1)

# game over buttons
Deadexit = button("exit", 525, 250, 1)
Deadresume = button("restart", 125, 250, 1)

# toolbar buttons
Pause = button("pause", 600, 0, 0.5)
ExitTool = button("exit", 700, 0, 0.5)
smallBack = button("back", 600, 0, 0.5)
smallBack2 = button("back", 600, 0, 0.5)
smallBack3 = button("back", 600, 0, 0.5)
smallBack4 = button("back", 600, 0, 0.5)
back = button("back", 700, 0, 0.5)

# in paused menu buttons
Resume = button("resume", 100, 250, 1)
RestartPause = button("restart", 325, 250, 1)
ExitPaused = button("exit", 550, 250, 1)
restartYes = button("confirm", (SCREEN_WIDTH // 2) - 90, 150, 1)
restartNo = button("back", (SCREEN_WIDTH // 2) - 90, 400, 1)

# username continue button
ContinueButton = button("continue", (SCREEN_WIDTH // 2) - 90, 500, 1)

# finished level buttons
newUser = button("newUser", (SCREEN_WIDTH // 2) - 90, 150, 1)
playAgain = button("playAgain", (SCREEN_WIDTH // 2) - 90, 300, 1)
leaderboard = button("leaderboard", (SCREEN_WIDTH // 2) - 90, 450, 1)

# username search buttons
search = button("search", (SCREEN_WIDTH // 2) - 90, 550, 1)
search2 = button("search", (SCREEN_WIDTH // 2) - 90, 550, 1)

# shop buttons
buyLeft = button("buy", 285, 480, 0.6)
buyRight = button("buy", 430, 480, 0.6)
shopback = button("exit", 460, 152, 0.5)

# creating instances of the main game classes
world = World()
player = world.process_data(world_data)

# main loop
run = True
while run:
    # setting the frame rate of the game
    clock.tick(FPS)

    # starting a timer
    begin_time = pygame.time.get_ticks() // 1000

    if gotUserName is False:
        # drawing background
        screen.blit(menuBG, (0, 0))
        # drawing texts
        screen.blit(text, textRect)
        screen.blit(text2, textRect2)
        # drawing input box and allowing for inputs
        pygame.draw.rect(screen, colour, inputRect, 3)
        text_surface = font.render(userText, True, (0, 0, 0))
        screen.blit(text_surface, (SCREEN_WIDTH // 2 - 60, SCREEN_HEIGHT // 2 + 10))
        inputRect.w = max(100, text_surface.get_width() + 20)

        with open("Leaderboard.txt", "r") as file:
            contents = file.read()

        # checking what colour the outline of the box needs to be
        if boxActive:
            colour = colourActive
        else:
            colour = colourPassive

        if userText != "":
            # checking if the input username is in accordance with the pattern
            if re.fullmatch(pattern, userText):
                clean = True
            else:
                clean = False

            if re.search(r'\b' + re.escape(userText) + r'\b', contents, re.MULTILINE):
                usernameTaken = True
        else:
            clean = True
            usernameTaken = False

        # is username taken

        # if it does not match pattern, error message is displayed
        if clean is False:
            errorMessage2 = font.render('Only AlphaNumerical Characters', True, (255, 255, 0))
            errorMessage2Rect = errorMessage2.get_rect()
            errorMessage2Rect.center = (SCREEN_WIDTH // 2, 450)
            screen.blit(errorMessage2, errorMessage2Rect)

        # checking is username is taken by looking in the file
        if usernameTaken:
            errorMessage = font.render('Username is Taken', True, (255, 255, 0))
            errorMessageRect = errorMessage.get_rect()
            errorMessageRect.center = (SCREEN_WIDTH // 2, 450)
            screen.blit(errorMessage, errorMessageRect)

        # checks if continue button was pressed and user entered a name. if so assign the relevant actions
        if ContinueButton.draw(screen) and not usernameTaken and clean and userText != "":
            gotUserName = True
            redrawWindow()
            fade()
            boxActive = False

    # Main menu code
    if game_start is False and gotUserName:

        # drawing BG
        screen.blit(menuBG, (0, 0))
        # checks which button is pressed
        if Start.draw(screen):
            delay = begin_time
            redrawWindow()
            fade()
            game_start = True
            fadeOutMenu = True
        if instruction.draw(screen):
            redrawWindow()
            fade()
            inInstruction = True
        if Menuexit.draw(screen):
            run = False

    # instruction page code
    if inInstruction:
        screen.blit(menuBG, (0, 0))
        # has user pressed back
        if back.draw(screen):
            redrawWindow()
            fade()
            inInstruction = False

        # instructions page text
        instructionTitle = font.render('Instructions', True, (0, 0, 0))
        instructionRect = instructionTitle.get_rect()
        instructionRect.center = (SCREEN_WIDTH // 2, 100)
        screen.blit(instructionTitle, instructionRect)

        instrucArray = ["The objectives of the game:", "Collect all coins before reaching the end",
                        "Avoid being killed by enemies", "Get to the end in the shortest amount of time"]
        height = 200
        for text in instrucArray:
            instructionTxt = arial.render(text, True, (0, 0, 0))
            instructionTxtRect = instructionTxt.get_rect()
            instructionTxtRect.center = (SCREEN_WIDTH // 2, height)
            screen.blit(instructionTxt, instructionTxtRect)

            height += 30

        screen.blit(mushroom_img, (SCREEN_WIDTH // 2 - 30, 400))
        screen.blit(kill_img, (SCREEN_WIDTH // 2 - 30, 500))

        powerArray = ["Higher jump for 10 seconds", "Instant Kill but can only be used once"]

        height = 460
        for text in powerArray:
            instructionTxt = arial.render(text, True, (0, 0, 0))
            instructionTxtRect = instructionTxt.get_rect()
            instructionTxtRect.center = (SCREEN_WIDTH // 2, height)
            screen.blit(instructionTxt, instructionTxtRect)

            height += 100

    # playing game code
    # checks if the user has entered username and if they pressed start
    if gotUserName and game_start:

        # calling main game functions
        draw_bg()
        world.draw()
        pause()
        player.update()
        player.draw()
        screen_scroll, player_lives = player.move(moving_left, moving_right)
        bg_scroll -= screen_scroll

        # calling item groups
        water_group.update()
        water_group.draw(screen)

        exit_group.update()
        exit_group.draw(screen)

        ice_barrier.update()
        ice_barrier.draw(screen)

        Item_group.update()
        Item_group.draw(screen)

        # start second timer for when the user enters the actual game
        start_time = pygame.time.get_ticks() // 1000

        # shop code
        if player.inShop:
            inShop = True
            if inShop:
                # drawing shop BG
                screen.blit(shopBG, (250, 150))
                if not shopMove:
                    if player.direction == 1:
                        player.rect.x -= 40
                        shopMove = True
                    else:
                        player.rect.x += 40
                        shopMove = True
                if shopback.draw(screen):
                    inShop = False
                    player.inShop = False
                    shopMove = False
                # does user have enough coins to buy something
                if player.coins >= 5:
                    # drawing buy btn
                    if buyLeft.draw(screen):
                        # user bought this item
                        screen.blit(black_img, (160, 5))
                        player.coins -= 5
                        boughtNuke = True
                        inShop = False
                        player.inShop = False
                        shopMove = False
                # does user have enough coins to buy something
                if player.coins >= 7:
                    if buyRight.draw(screen):
                        # user bought this item
                        screen.blit(black_img, (160, 5))
                        player.coins -= 7
                        # Items.boughtUnlock = True
                        for ice in ice_barrier:
                            ice.kill()
                        inShop = False
                        player.inShop = False
                        shopMove = False
                # if the user bought the nuke, kill all enemies
                if boughtNuke:
                    for enemySprite in enemy_group:
                        enemySprite.kill()

        # sound for game over
        if player.lives == -1:
            gameOverSound.set_volume(1)
            gameOverSound.play(0)

        # looping through enemy instances and calling their draw and enemy (AI) functions
        for enemySprite in enemy_group:
            enemySprite.enemies()
            enemySprite.update()
            enemySprite.draw()

        for enemySprite in enemy_group:
            if pygame.sprite.collide_rect(enemySprite, player):
                if enemySprite.direction == player.direction:
                    if attacking:
                        if player.superSword:
                            enemySprite.enemyLives = 0
                        else:
                            enemySprite.enemyLives -= 1
                        if enemySprite.enemyLives == 0:
                            enemySprite.alive = False
                        if player.superSword:
                            DeadEnemySound.play()
                            screen.blit(powerText, (300, 0))
                            DeadEnemySound.play()
                            # setting attacking variables to default values
                            enemySprite.alive = False
                            player.superSword = False
                        if player.direction == 1:
                            player.rect.x -= 40
                            enemySprite.rect.x += 40
                        else:
                            player.rect.x += 40
                            enemySprite.rect.x -= 40
                else:
                    if not player.superSword:
                        player.lives -= 0.25
                        if player.direction == 1:
                            player.rect.x -= 40
                            enemySprite.rect.x += 40
                        else:
                            player.rect.x += 40
                            enemySprite.rect.x -= 40
                    if player.superSword:
                        enemySprite.enemyLives -= 1
                        if player.direction == 1:
                            player.rect.x -= 40
                            enemySprite.rect.x += 40
                        else:
                            player.rect.x += 40
                            enemySprite.rect.x -= 40

        # toolbar heart images that show the player lives depending on current lives
        if player.lives == 3:
            screen.blit(threeLives, (5, 7))
        if player.lives == 2:
            screen.blit(twoLives, (5, 7))
        if player.lives == 1:
            screen.blit(oneLives, (5, 7))
        if player.lives <= 0:
            player.alive = False
            screen.blit(NoLives, (5, 7))

        # animation update depending on movement type
        if player.alive:
            if player.in_air:
                player.update_action(2)
            elif moving_left or moving_right:
                player.update_action(1)
            elif attacking:
                player.update_action(3)
            else:
                player.update_action(0)

        # if user falls in water
        if player.alive is False and player.gameOver is False:
            player.coins = 0
            restart = True
            restartNow()

    # if user lost all lives
    if player.gameOver:
        # draw new BG
        # draw game over text
        screen.blit(menuBG, (0, 0))
        screen.blit(text4, textRect4)
        # has user clicked exit
        if Deadexit.draw(screen):
            run = False
        # has user clicked restart
        if Deadresume.draw(screen):
            # calling restart functions
            redrawWindow()
            fade()
            player.coins = 0
            restart = True
            restartNow()

    # toolbar code and functionality
    if game_start and player.gameOver is False and inLeaderboard is False and paused is False:
        # drawing coin counter
        screen.blit(coin_counter, (120, 5))
        text3 = arial.render(str(player.coins), True, WHITE)
        arialRect = text3.get_rect()
        arialRect.center = (175, 22)
        screen.blit(text3, arialRect)

        # creating barrier between game and toolbar at the top
        pygame.draw.line(screen, (65, 88, 133), (0, 40), (800, 40), 5)

        # has user pressed pause
        if Pause.draw(screen):
            time_enter = pygame.time.get_ticks() // 1000
            paused = True
            InPaused = True
        # has user pressed exit
        if ExitTool.draw(screen):
            run = False

    # pause menu code
    if InPaused and confirmRestart is False and player.finishedGame is False:
        # has user restarted in pause menu
        if RestartPause.draw(screen):
            confirmRestart = True
            InPaused = False
        # resume back to game play
        if Resume.draw(screen):
            time_leave = pygame.time.get_ticks() // 1000
            startCalc = True
            paused = False
            InPaused = False
        # exit game
        if ExitPaused.draw(screen):
            run = False

    # pause menu restart confirmation for the user code
    if confirmRestart:
        inPaused = False
        # has user confirmed
        if restartYes.draw(screen):
            # call restart functions
            # reset game variables to default
            time_leave = pygame.time.get_ticks() // 1000
            backToLevel1 = True
            player.completedLevel = False
            player.finishedGame = False
            startCalc = True
            InPaused = False
            paused = False
            restartInPause = True
            confirmRestart = False
            player.inShop = False
            inShop = False
            shopMove = False
            redrawWindow()
            fade()
            player.coins = 0
            restartNow()
        # has user not confirmed
        # go back to the pause menu home page
        if restartNo.draw(screen):
            confirmRestart = False
            InPaused = True

    # new level
    if backToLevel1:
        level = 1
        backToLevel1 = False
    # is user on the 2nd level and has completed the game. if so set relevant variables to true
    if level == 2 and player.completedLevel:
        player.finishedGame = True
        player.gameOver = True
    # is user on 1st level and have they completed it. if so increment level and call restart function
    if player.completedLevel and level == 1:
        level += 1
        restart = True
        restartNow()

    # calculates times that user spent in the pause menu and appends to array
    if startCalc:
        timeNotPlaying = time_leave - time_enter
        timesRemove.append(timeNotPlaying)
        startCalc = False

    # if the user finished the level, the final time is calculated
    if player.gameOver and player.completedLevel and level == 2:
        finalTime = start_time
        lengthArr = len(timesRemove)
        # loops around pause times in the array and deducts from final time to get an accurate time for time spent paying
        for i in range(0, lengthArr):
            finalTime -= timesRemove[i]
        finalTime = finalTime - delay
        player.completedLevel = False

    # checks if the fade screen has finished before starting another timer
    if fadeOutMenu:
        notInMenu = True
        current_Time = pygame.time.get_ticks()

    # if the user collects a mushroom --> powerup functionality
    if player.jumpPower:
        # reduce gravity, hence easier to jump higher
        Gravity = 0.25
        # set timer for 5 seconds and then call function to rest gravity to default
        timer = Timer(5, gravReset)
        # start timer
        timer.start()
        player.jumpPower = False

    # finished level menu code
    if player.finishedGame:
        # draw BG
        screen.blit(menuBG, (0, 0))

        # draw relevant text
        endText = font.render("You have completed the Level", True, (0, 0, 0))
        endTextRect = endText.get_rect()
        endTextRect.center = (SCREEN_WIDTH // 2, 100)

        screen.blit(endText, endTextRect)

        if displayedLeaderboard is False:
            # changing solo digit times to double digit to allow for comparisons
            if int(finalTime) < 10:
                finalTime = str(finalTime).zfill(2)
            # open leader board file
            leaderBoard = open("Leaderboard.txt", "a")
            # write user time next to their username and add a new line
            tab = "\t"
            newLine = "\n"
            leaderBoard.write(f"{str(finalTime)}{tab}{tab}{tab}{userName}{newLine}")
            # close file
            leaderBoard.close()
            displayedLeaderboard = True
        # has user selected new user
        if newUser.draw(screen):
            # restart game from the start and allow a new user to play with their own username
            redrawWindow()
            fade()
            gotUserName = False
            game_start = False
            player.finishedGame = False
            player.completedLevel = False
            displayedLeaderboard = False
            level = 1
            userText = ""
            username = ""
            begin_time = pygame.time.get_ticks() // 1000
            player.coins = 0
            player_lives = 3
            restartNow()
        # play again button
        if playAgain.draw(screen):
            # restart game with the same user and username
            backToLevel1 = True
            player.completedLevel = False
            player.finishedGame = False
            player.inShop = False
            inShop = False
            shopMove = False
            redrawWindow()
            fade()
            player.coins = 0
            restartNow()
        # leaderboard button
        if leaderboard.draw(screen):
            # if btn is pressed, read files in rising order into the ranked array
            leaderBoard = open("Leaderboard.txt", "r")
            readingFile = leaderBoard.readlines()
            ranked = sorted(readingFile, reverse=False)
            redrawWindow()
            fade()
            # setting leader board var to true to indicate we are in the leader board
            inLeaderboard = True
            player.finishedGame = False

    # top scores page at the end, code
    if inLeaderboard:
        screen.blit(menuBG, (0, 0))
        screen.blit(text5, textRect5)
        # exit btn
        if ExitTool.draw(screen):
            run = False
        # back btn to menu
        if smallBack.draw(screen):
            redrawWindow()
            fade()
            player.finishedGame = True
            inLeaderboard = False
        # search btn for searching usernames that are not in the leaderboard
        if search.draw(screen):
            redrawWindow()
            fade()
            inLeaderboard = False
            inLookingUsername = True

        # drawing leaderboard box
        pygame.draw.rect(screen, (0, 0, 0), (210, 150, 380, 60), 3)
        pygame.draw.rect(screen, (0, 0, 0), (210, 208, 380, 310), 3)
        height = 175
        LBtitle = font.render("TIME                 NAME", True, (0, 0, 0))
        LBRect2 = text2.get_rect()
        LBRect2.center = (SCREEN_WIDTH // 2, height)
        screen.blit(LBtitle, LBRect2)
        height += 75
        # looping through the ranked array and drawing top scores into the leader board box
        for line in range(0, 4):
            CurrentLine = ranked[line]
            LB = font.render(str(CurrentLine), True, (0, 0, 0))
            LBRect = text2.get_rect()
            LBRect.center = (SCREEN_WIDTH // 2, height)
            screen.blit(LB, LBRect)
            height += 75

    # username search page code
    if inLookingUsername:
        screen.blit(menuBG, (0, 0))

        clean = False

        # exit game
        if ExitTool.draw(screen):
            run = False
        # back to menu
        if smallBack2.draw(screen):
            redrawWindow()
            fade()
            found = False
            inLookingUsername = False
            searchText = ""

        # relevant text
        searchTitle = font.render('Enter username you wish to look up', True, (0, 0, 0))
        searchRect2 = searchTitle.get_rect()
        searchRect2.center = (SCREEN_WIDTH // 2, 200)
        screen.blit(searchTitle, searchRect2)
        # drawing input box
        pygame.draw.rect(screen, colour, searchRect, 3)
        text_surface = font.render(searchText, True, (0, 0, 0))
        screen.blit(text_surface, (SCREEN_WIDTH // 2 - 60, 300 + 10))
        searchRect.w = max(100, text_surface.get_width() + 20)

        # changing input box outline
        if boxActive:
            colour = colourActive
        else:
            colour = colourPassive

        if searchText != "":
            # checking if the input username is in accordance with the pattern
            if re.fullmatch(pattern, searchText):
                clean = True
            else:
                errorMessage2 = font.render('Only AlphaNumerical Characters', True, (255, 255, 0))
                errorMessage2Rect = errorMessage2.get_rect()
                errorMessage2Rect.center = (SCREEN_WIDTH // 2, 450)
                screen.blit(errorMessage2, errorMessage2Rect)

        # search for the input username
        if search2.draw(screen) and clean:
            redrawWindow()
            fade()
            inLeaderboard = False
            inLookingUsername = False
            searchResult = True

    # username search result page
    if searchResult:
        # draw BG
        screen.blit(menuBG, (0, 0))
        # exit
        if ExitTool.draw(screen):
            run = False
        # back to menu
        if smallBack3.draw(screen):
            redrawWindow()
            fade()
            searchResult = False
            inLeaderboard = True
            searchText = ""
            found = False
            rank = 0
            screen.blit(menuBG, (0, 0))

        # open leader board file
        file = open("Leaderboard.txt", "r")
        leaderboardContent = file.read()

        # if it is found loop again to fnd the index the name is (the index is the rank number as this is the ranked array)
        if re.search(r'\b' + re.escape(searchText) + r'\b', leaderboardContent, re.MULTILINE):
            lenRanked = len(ranked)
            for i in range(0, lenRanked - 1):
                if searchText in ranked[i]:
                    rank = i + 1
            # relevant text to inform user of the rank of the username
            rankTitle = font.render(f"{str(searchText)} is ranked {str(rank)}", True, (0, 0, 0))
            rankTitleRect2 = rankTitle.get_rect()
            rankTitleRect2.center = (SCREEN_WIDTH // 2, 200)
            screen.blit(rankTitle, rankTitleRect2)
        else:
            rankTitle = font.render(f"{str(searchText)} has not been registered in the game", True, (0, 0, 0))
            rankTitleRect2 = rankTitle.get_rect()
            rankTitleRect2.center = (SCREEN_WIDTH // 2, 200)
            screen.blit(rankTitle, rankTitleRect2)

    # event checkers / handlers
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            run = False
        if InPaused is False and confirmRestart is False and player.alive and not inShop:
            # key presses
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    paused = True
                    InPaused = True
                if event.key == pygame.K_LEFT or event.key == pygame.K_a:
                    moving_left = True
                if event.key == pygame.K_RIGHT or event.key == pygame.K_d:
                    moving_right = True
                if event.key == pygame.K_UP or event.key == pygame.K_SPACE or event.key == pygame.K_w:
                    player.jump = True
                    jumpSound.set_volume(0.2)
                    jumpSound.play()
                if event.key == pygame.K_k:
                    attacking = True
        # key presses
        if event.type == pygame.KEYDOWN:
            if boxActive:
                if gotUserName is False:
                    if event.key == pygame.K_BACKSPACE:
                        userText = userText[0:-1]
                    else:
                        userText += event.unicode
                elif inLookingUsername and boxActive:
                    if event.key == pygame.K_BACKSPACE:
                        searchText = searchText[0:-1]
                    else:
                        searchText += event.unicode
            else:
                userName = userText
        # has user released the key
        if event.type == pygame.KEYUP:
            if event.key == pygame.K_LEFT or event.key == pygame.K_a:
                moving_left = False
            if event.key == pygame.K_RIGHT or event.key == pygame.K_d:
                moving_right = False
            if event.key == pygame.K_k:
                attacking = False
        # mouse click
        if event.type == pygame.MOUSEBUTTONDOWN:
            if inputRect.collidepoint(event.pos) or searchRect.collidepoint(event.pos):
                boxActive = True
            else:
                boxActive = False

    # update screen
    pygame.display.update()

# quit game (exit function)
pygame.quit()
